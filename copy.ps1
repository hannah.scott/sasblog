rm -Recurse -Force ((Get-Date -Format 'yyyy-MM-dd') + '_html')
mkdir ((Get-Date -Format 'yyyy-MM-dd') + '_html') > $null
cp public/*.html ((Get-Date -Format 'yyyy-MM-dd') + '_html')
rm -Recurse -Force public/*.html
mv markdown/*.html markdown/html
cp markdown/html/*.html public
