# SAS cheatsheet

[Home](index.html)

---

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [SAS cheatsheet](#sas-cheatsheet)
	- [Sampling and partitions](#sampling-and-partitions)
		- [Simple random sampling](#simple-random-sampling)
	- [Macros](#macros)
		- [Using a macro file with `%include`](#using-a-macro-file-with-include)
		- [Macro `%do` loops](#macro-do-loops)
		- [Macro `%while` loops](#macro-while-loops)
	- [`proc expand`](#proc-expand)
		- [Moving averages](#moving-averages)
		- [`retain`-like functionality](#retain-like-functionality)

<!-- /TOC -->

## Sampling and partitions

### Simple random sampling

To randomly sample a dataset with a certain percentage, use `ranuni`:

```sas
data example_pct10;
    set example;
    where ranuni(0) < 0.1;
run;
```

If you need a specific number of rows use `proc surveyselect`:

```sas
proc surveyselect
    data=example
    out=sample
    method=srs  /* Simple random sampling */
    n=1000;     /* Rows needed */
run;
```

## Macros

### Using a macro file with `%include`

If you have a macro file at `/path/to/file/macros.sas` you can import and use
those macros in any SAS file by putting

```sas
%include "/path/to/file/macros.sas";
```

at the top of your file. This is useful if you have a header file too, e.g. a
file that sets all the libnames you need.

### Macro `%do` loops

The syntax for a `%do` loop (a `do` loop where the counter is a macrovariable)
is

```sas
%macro loop;
  %do i=1 %to 5;
    /* Do something with macrovariable &i. */
    %put &i.;
  %end;
%mend;
```

The advantage of this over a regular `do` loop is that you can loop datasteps,
e.g. to create multiple tables.

### Macro `%while` loops

```sas
%macro loop;
  %let i=1;
  %do %while (&i. <= 5);
    /* Do something with macrovariable &i. */
    %put &i.;
    /* Increment &i. */
    %let i = %eval(&i. + 1);
  %end;
%mend;
</pre>
```

## `proc expand`

### Moving averages

For a simple three-point moving average, use the `movave` transform:

```sas
proc expand data=example;
  by x y;
  id t;
  convert x_t = MA / transout=(movave 3);
run;
```

If you want a three-point moving average _not_ including the current point (so
the average of _x-1, x-2, x-3_) you can hack it with weighted averages:

```sas
proc expand data=example;
  by x y;
  id t;
  convert x_t = MA / transout=(movave (1 1 1 0));
run;
```

### `retain`-like functionality

You can get the previous entry by using the `movave` transform:

```sas
proc expand data=example;
  by x y;
  id t;
  convert x_t = prev_x_t / transout=(movave (1 0));
run;
```

For the next entry you can combine this with the `reverse` transform:

```sas
proc expand data=example;
  by x y;
  id t;
  convert x_t = next_x_t / transout=(
    reverse
    movave (1 0)
    reverse
  );
run;
```
