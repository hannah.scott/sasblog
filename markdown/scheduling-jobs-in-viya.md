# Scheduling jobs in Viya

[Home](index.html)

---

This assumes you've got some code that runs in SAS Studio without any problems.

**NB:** We're going to assume your SAS Studio instance is hosted as _sasviya.local_ and the CAS server is at _sasviyacas.local:5570_. Your actual config may vary.

1. Navigate to _sasviya.local/SASJobExecution_
2. Go to _Content > Users > [user] > My Folder > SASJobExecution_
  - content looks like a folder with a DB symbol over it
3. Right-click and create a new file in _SASJobExecution_: a prompt should open
4. This is going to be your SAS job, so name it whatever your working code is called and make sure _File type_ is set to **Job definition** and _Server_ is set to the server you want to run it on
5. Edit your new file and paste your code in
  - your user autoexec files will not run, so paste anything you need to in here
  - if you're using CAS, you will need to manually assign a `cashost` and `casport` at the top of your code, for example:
```
options cashost="sasviyacas.local" casport=5570;
cas;
caslib _all_ assign;
```
6. Close the editor and _run_ the code to check everything is working: go to _Jobs_ (gear symbol) and check that the job appears here and runs successfully
7. Right-click on your successful job and click _Schedule job_: this will open a dialog
8. Add a name and description to your scheduled job, then click _Add a new trigger_
9. Configure trigger settings and _Save_
10. Right-click your scheduled job and _run_ it to test that it still works
